((Drupal, drupalSettings, once) => {
  'use strict';

  /**
   * Prevent a click.
   */
  function preventDefault(e) {
    e.stopPropagation();
    e.preventDefault();
    return false;
  }

  /**
   * Uses window.postMessage() to send UI clicks to the parent window.
   *
   * @param Event e
   *   The click event.
   */
  function lpbClickHander(e) {
    // First, send the ajaxPageState to the parent window.
    window.parent.postMessage({
      type: 'ajaxPreviewPageState',
      settings: drupalSettings.ajaxPageState
    });
    // Remove "is-me-focused" class from all elements.
    document.querySelectorAll('.is-me-focused').forEach((el) => {
      el.classList.remove('is-me-focused');
    });
    // Add "is-me-focused" class to the clicked element.
    e.currentTarget.classList.add('is-me-focused');
    // Then, send the click event.
    const message = {
      type: 'lpbUiClick',
      settings: {
        dialogType: e.currentTarget.getAttribute('data-dialog-type'),
        dialog: JSON.parse(e.currentTarget.getAttribute('data-dialog-options')),
        dialogRenderer: JSON.parse(e.currentTarget.getAttribute('data-dialog-renderer')),
        url: e.currentTarget.getAttribute('href'),
      }
    }
    window.parent.postMessage(message);
    e.stopPropagation();
    e.preventDefault();
    return false;
  }

  /**
   * Attaches the behavior to the edit screen.
   */
  Drupal.behaviors.mercuryEditorPreviewScreen = {
    attach: function(context, _settings) {
      // Send the initial ajaxPageState to the parent window.
      window.parent.postMessage({
        type: 'ajaxPreviewPageState',
        settings: drupalSettings.ajaxPageState
      });
      // Attaches click handlers to links that use window.postMessage().
      once('me-msg-broadcaster', '.use-postmessage').forEach((el) => {
        el.addEventListener('mousedown', preventDefault);
        el.addEventListener('mouseup', preventDefault);
        el.addEventListener('click', lpbClickHander);
      });
      // Prevent links from working in iframe.
      if (window.parent !== window) {
        once('me-stop-iframed-links', 'a', context).forEach((link) => {
          if (link.closest('.lpb-controls') === null) {
            link.setAttribute('target', '_parent');
            link.addEventListener('click', (e) => {
              e.stopPropagation();
              e.preventDefault();
              return false;
            });
          }
        });
        once('me-prevent-focus', 'a, button, input, textarea, select, details', context).forEach((focussable) => {
          if (
            focussable.closest('.lpb-controls') === null &&
            !focussable.classList.contains('use-postmessage')
          ) {
            focussable.setAttribute('tabindex', '-1');
          }
        });
      }
    }
  }
})(Drupal, drupalSettings, once)
