(($, Drupal, once) => {

  /**
   * Ensures that all layout paragraphs controls are fully within viewport.
   *
   * @param {jQuery} $builder
   *   The Layout Paragraphs container jQuery object.
   */
  function repositionControls($builder) {
    $builder.find('.lpb-controls').each((i, controls) => {
      controls.setAttribute('style', controls.getAttribute('data-style'));
      const bounding = controls.getBoundingClientRect();
      // Left viewport edge.
      const l = 0;
      // Right viewport edge.
      const r = (window.innerWidth || document.documentElement.clientWidth);
      // Overlapping left.
      if (bounding.left < l) {
        controls.setAttribute('data-style', controls.getAttribute('style'));
        $(controls).offset({left: 0});
      }
      // Overlapping right.
      if (bounding.right > r) {
        $(controls).css({right: (bounding.right - r) + 'px'});
      }
    });
  }

  /**
   * Simplifies drag and drop visual cues to prevent jumpiness.
   *
   * The default behavior of the dragula library can create excessive
   * jumpiness in some cases. This function simplifies the UI and drag and drop
   * experience in several key ways, including:
   *
   * - Detaches all layout paragraphs UI elements when dragging starts.
   * - Provides a simple "hint" element to show where an item will be dropped.
   * - Leaves a "ghost" copy of the grabbed element in place at the source.
   * - Reattaches all UI elements when dragging ends.
   *
   * @see https://github.com/bevacqua/dragula#drakeon-events.
   *
   * @param {Object} drake
   *   The dragula object.
   */
  function simplifyDragHints($builder, settings) {

    const drake = $builder.data('drake');
    let ghost, grabbed;
    const hint = $('<div class="lp-hint hidden"></div>')[0];

    // Hide UI elements when dragging starts.
    drake.on('drag', (el) => {
      el.parentNode.insertBefore(hint, el);
      $builder.find('.js-lpb-ui').addClass('hidden');
    });
    // Provide a simple hint element to indicate where an item will be dropped.
    drake.on('shadow', (item, container) => {
      if (item.classList.contains('lp-hint')) {
        return;
      }
      // Remove comments and text nodes from container.
      [...container.childNodes].filter((e) => e.classList === undefined).forEach((e) => e.remove());
      container.replaceChild(hint, item);

      // Ensure the hint does not get at the end of the region after the add button.
      if (hint.nextSibling === null && hint.previousSibling !== null && hint.previousSibling.classList.contains('lpb-btn--add')) {
        container.insertBefore(hint, hint.previousSibling);
      }

      const nextIsGhost = hint.nextSibling !== null ? hint.nextSibling.classList.contains('lp-ghost') : false;
      const prevIsGhost = hint.previousSibling !== null ? hint.previousSibling.classList.contains('lp-ghost') : false;
      const ghostAdjacent = nextIsGhost || prevIsGhost;
      if (ghostAdjacent) {
        hint.classList.add('hidden');
        ghost.classList.remove('gu-transit');
      }
      else {
        hint.classList.remove('hidden');
        ghost.classList.add('gu-transit');
      }
    });
    // Leave a copy of the grabbed item in place at the original source.
    drake.on('cloned', (mirror, item) => {
      ghost = item.cloneNode(true);
      ghost.classList.add('lp-ghost');
      item.parentNode.insertBefore(ghost, item);
      grabbed = item;
      item.remove();
    });
    // Show UI elements and remove ghost and hint elements when dragging stops.
    drake.on('dragend', (el) => {
      hint.replaceWith(grabbed);
      ghost.remove();
      $builder.find('.js-lpb-ui').removeClass('hidden');
      repositionControls($builder);
      $builder.trigger('lpb-component:drop', [el.getAttribute('data-uuid')]);
    });
  }

  /**
   * Waits for a condition to be met, then calls the provided callback.
   *
   * @param {Function} cond
   *   The condition to wait for.
   * @param {Function} cb
   *   The callback to call when cond evaluates true.
   */
  function waitFor(cond, cb) {
    const i = setInterval(() => {
      if (cond() === true) {
        clearInterval(i);
        cb();
      }
    }, 100);
  }

  function removeMovedFormActions() {
    const outdatedFormActions = document.getElementsByClassName('lpb-form__actions repositioned');
    if (outdatedFormActions.length) {
      outdatedFormActions[0].remove();
    }
  }

  function moveFormActions(context) {
    const formActions = context.querySelector('.lpb-form__actions');
    if (formActions) {
      removeMovedFormActions();
      formActions.classList.add('repositioned');
      document.body.appendChild(formActions);
    }
  }

  function showControls(e) {
    const el = e.target.closest('.lpb-controls, .js-lpb-component');
    el.classList.add('focused');
    el.classList.remove('transitioning');
    el.classList.remove('blurred');
  }

  function hideControls(e) {
    const el = e.target.closest('.lpb-controls, .js-lpb-component');
    el.classList.add('transitioning');
    setTimeout(() => {
      if (el.classList.contains('transitioning')) {
        el.classList.remove('focused');
        el.classList.remove('transitioning');
        el.classList.add('blurred');
      }
    }, 250);
  }

  Drupal.behaviors.mercuryEditorDragula = {
    attach: function attach(context, settings) {
      // Append form-actions to body for better styling control.
      if (once('me-builder-events', 'html', context).length) {
        $(window).on('lpb-builder:open.lpb lpb-builder:save.lpb', (e) => {
          moveFormActions(context);
        });
        $(window).on('lpb-builder:close.lpb', (e) => {
          removeMovedFormActions();
        });
      }
      var events = ['lpb-component:insert.lpb', 'lpb-component:update.lpb', 'lpb-component:move.lpb', 'lpb-component:drop.lpb'].join(' ');
      $(once('me-builder-form', '[data-lpb-id]', context)).on(events, function (e) {
        const cancelButton = document.querySelector('lpb-form__actions repositioned .lpb-btn--cancel');
        if (cancelButton) {
          cancelButton.value = Drupal.t('Cancel')
        }
      });
      once('me-dragula', '.lp-builder.has-components').forEach((builder) => {
        const $builder = $(builder);
        waitFor(
          () => $builder.data('drake') !== undefined,
          () => {
            repositionControls($builder);
            moveFormActions($builder[0]);
            $(window).resize(() => repositionControls($builder));
            // Simplifies drag and drop functionality.
            try {
              simplifyDragHints($builder, settings);
            }
            catch (e) {
              console.warn(e);
            }
          });
      });
      once('reveal-on-hover', '.js-lpb-component').forEach((component) => {
        component.addEventListener('mouseenter', showControls);
        component.addEventListener('mouseleave', hideControls);
      });
      once('reveal-on-hover', '.lpb-controls').forEach((el) => {
        el.addEventListener('mouseenter', showControls);
        el.addEventListener("focusin", showControls);
        el.addEventListener('mouseleave', hideControls);
        el.addEventListener("focusout", hideControls);
      });
    }
  }
})(jQuery, Drupal, once);
