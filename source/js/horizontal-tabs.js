(($, Drupal, once) => {
  function toggleTabs(selectedValue) {
    (document.querySelectorAll('.me-tab-group') || []).forEach((tabGroup) => {
      tabGroup.setAttribute('aria-hidden', true);
      tabGroup.classList.add('hidden-tab');
    });
    (document.querySelectorAll(`.me-tab-group--${selectedValue}`) || []).forEach((tabGroup) => {
      tabGroup.removeAttribute('aria-hidden');
      tabGroup.classList.remove('hidden-tab');
    });
  }

  Drupal.behaviors.mercuryEditorTabs = {
    attach: function(settings, context) {
      once('me-tabs', '.me-tabs input[type="radio"]').forEach((input) => {
        input.addEventListener('change', (event) => {
          toggleTabs(input.value);
        });
      });
      document.querySelectorAll('.me-tabs').forEach((tabs) => {
        const selected = tabs.querySelector('input[type="radio"]:checked') || {};
        toggleTabs(selected.value);
      });
    }
  }

  /**
   * Repositions horizontal tabs into the header of a modal dialog.
   */
  $(window).on('dialog:aftercreate', (event, dialog, $dialog) => {
    const id = $dialog.attr('id');
    if (id && id.indexOf('lpb-dialog-') === 0) {
      const $tabs = $dialog.find('.horizontal-tab-radios');
      const $titlebar = $dialog.closest('.ui-dialog').find('.ui-dialog-titlebar');
      if ($tabs.length && $titlebar.length) {
        $titlebar
          .addClass('has-tabs')
          .append($tabs);
      }
    }
  });

})(jQuery, Drupal, once);
