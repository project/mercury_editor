<?php

namespace Drupal\mercury_editor;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\mercury_editor\Ajax\IFrameCommandsWrapperCommand;

class AjaxResponseIframeAdapter {

  protected $defaultTheme;

  protected $updatingIframe = FALSE;

  protected $themeManager;

  public function __construct(ThemeManagerInterface $theme_manager, ConfigFactoryInterface $config_factory, ThemeInitializationInterface $theme_initializer) {
    $default_theme_name = $config_factory->get('system.theme')->get('default');
    $this->defaultTheme = $theme_initializer->getActiveThemeByName($default_theme_name);
    $this->themeManager = $theme_manager;
  }

  public function updatingIframe(){
    $this->updatingIframe = TRUE;
    $this->themeManager->setActiveTheme($this->defaultTheme);
  }

  public function isUpdatingIframe() {
    return $this->updatingIframe;
  }

  public function ajaxRenderAlter(&$data) {
    if ($this->updatingIframe) {
      $wrapper_commands = array_filter($data, function ($command) {
        return $command['command'] !== 'closeMercuryDialog';
      });
      $data = array_filter($data, function ($command) {
        return $command['command'] == 'closeMercuryDialog';
      });
      $wrapper_command = new IFrameCommandsWrapperCommand($wrapper_commands);
      $data[] = $wrapper_command->render();
    }
    return $data;
  }

}
