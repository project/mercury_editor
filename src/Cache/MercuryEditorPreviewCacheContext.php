<?php

namespace Drupal\mercury_editor\Cache;

use Drupal\Core\Cache\Context\RouteNameCacheContext;

/**
 * Determines if an entity is being viewed in Mercury Editor Preview.
 *
 * Cache context ID: 'route.name.is_mercury_editor_preview'.
 *
 * @internal
 *   Tagged services are internal.
 */
class MercuryEditorPreviewCacheContext extends RouteNameCacheContext {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Mercury Editor preview');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $route_name = $this->routeMatch->getRouteName();
    $is_preview = (int) (
      $route_name == 'mercury_editor.preview'
      || str_ends_with($route_name, '.mercury_editor_preview')
    );
    $context_value = 'is_mercury_editor_preview.' . $is_preview;
    return $context_value;
  }

}
