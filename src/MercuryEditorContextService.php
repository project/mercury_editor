<?php

namespace Drupal\mercury_editor;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a service for Mercury Editor context.
 */
class MercuryEditorContextService {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * MercuryEditorContextService constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * Determines if the current route is a "Mercury Editor" preview.
   *
   * This involves checking if a route name exists and is either exactly
   * 'mercury_editor.preview' or ends with '.mercury_editor_preview'.
   *
   * @return bool
   *   Returns TRUE if the current route is a Mercury Editor preview, else
   *   FALSE.
   */
  public function isPreview(): bool {
    return ($route_name = $this->routeMatch->getRouteName())
      && (
        $route_name === 'mercury_editor.preview'
        || str_ends_with($route_name, '.mercury_editor_preview')
      );
  }

  /**
   * Determines if the current route is a "Mercury Editor" editor.
   *
   * The method checks the route name retrieved from the route match object and
   * compares it to 'mercury_editor.editor'. If the route name is exactly
   * 'mercury_editor.editor', the method returns true, indicating that the
   * current route is indeed for a "Mercury Editor" editor. Otherwise, it
   * returns false.
   *
   * @return bool
   *  Returns TRUE if the current route is for a "Mercury Editor" editor; FALSE
   *  otherwise.
   */
  public function isEditor(): bool {
    $route_name = $this->routeMatch->getRouteName();
    return $route_name === 'mercury_editor.editor';
  }

  /**
   * Returns the mercury editor preview entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The mercury editor entity.
   */
  public function getEntity(): ?ContentEntityInterface {
    if ($this->isEditor()) {
      return $this->routeMatch->getParameter('mercury_editor_entity');
    }

    if ($this->isPreview() && $route_name = $this->routeMatch->getRouteName()) {
      $entity_type = explode('.', $route_name)[1];
      return $this->routeMatch->getParameter($entity_type);
    }

    return NULL;
  }

}
